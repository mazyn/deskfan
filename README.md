This is a poweruser script for Fandom's Zendesk instance.

## Import
This script is used by importing main.js via your prefered method, as well as an optional dark theme (dark.user.css) that you can import via stylus or etc.

## Config
The script can be configured by using the following options, as shown in the codeblock:
* `user` - your Fandom username (defaults to nothing/empty if not set)
* `info` - your device's info (defaults to your useragent if not set)
```js
window.deskfan = {
    user: 'My cool username',
    info: 'Firefox on Windows 10'
}
```

## Credits
* [KockaAdmiralac](https://kocka.tech) (Original code/idea)
* [Freepik](https://www.flaticon.com/authors/freepik) (Icon from [flaticon](http://www.flaticon.com/))
* [Puxlit](https://puxlit.net) (Came up with the name)
* [Dorumin](https://github.com/Dorumin) (Spanish translation)
* [fngplg](https://community.fandom.com/wiki/User:Fngplg) (Russian translation)