(() => {
    'use strict';

    //Set up config and i18n
    const config = {
        user: '',
        info: window.navigator.userAgent,
        ...window.deskfan
    };
    const lang = document.documentElement.getAttribute('lang');
    const i18n = {
        'en-US': {
            general: 'GENERAL',
            feedback: 'FEEDBACK',
            bug: 'BUG',
            prefer: 'english_'
        },
        es: {
            general: 'GENERAL',
            feedback: 'COMENTARIOS',
            bug: 'BUG',
            prefer: 'español'
        },
        ru: {
            general: 'Оставить запрос',
            feedback: 'Оставить отзыв',
            bug: 'Сообщить об ошибке',
            prefer: 'русский'
        }
    }[lang];

    //Insert header links
    const link1 = document.createElement('a');
    link1.innerText = i18n.general;
    link1.href = '/hc/requests/new?ticket_form_id=360000956114';
    const link2 = document.createElement('a');
    link2.innerText = i18n.feedback;
    link2.href = '/hc/requests/new?ticket_form_id=360000940393&tf_360017952614=i_don_t_see_an_option_for_my_issue';
    const link3 = document.createElement('a');
    link3.innerText = i18n.bug;
    link3.href = '/hc/requests/new?ticket_form_id=360000940393&tf_360017952614=i_have_found_a_bug';
    const container = document.createElement('span');
    container.id = 'link-container';
    container.append(link1, link2, link3);
    const styles = document.createElement('style');
    styles.innerHTML = `
        .header #link-container a:not(:last-child) {
            padding-right: 15px;
        }
    `;
    document.head.append(styles);
    document.querySelector('.header > .logo').insertAdjacentElement('afterend', container);

    //Set fields that we prefill
    if (document.querySelector('#new_request')) {
        document.querySelector('#request_custom_fields_360017969174').value = config.user;
        document.querySelector('#request_custom_fields_360018001913').value = i18n.prefer;
        document.querySelector('#request_custom_fields_360016533274').value = config.info;
    }

    //Hide suggested articles
    if (document.querySelector('#new_request')) {
        const el = document.createElement('style');
        el.innerHTML = `
            #new_request .suggestion-list {
                display: none;
            }
        `;
        document.head.append(el);
    }

    //Make time shown how it would be shown locally
    if (document.querySelector('.request-details time') && lang === 'en-US') {
        document.querySelectorAll('.request-details time').forEach(el => {
            const timestamp = new Date(el.getAttribute('datetime'));
            const newTimestamp = timestamp.toLocaleTimeString('en-US', { timeStyle: 'short' });
            el.innerText = el.innerText.replace(/\d\d:\d\d$/, newTimestamp);
        });
    }
})();